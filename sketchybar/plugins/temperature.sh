#!/bin/sh

TEMP=$(~/.config/sketchybar/plugins/temp_sensor)

# The item invoking this script (name $NAME) will get its icon and label
# updated with the current battery status
sketchybar --set $NAME icon= label="${TEMP}°"
