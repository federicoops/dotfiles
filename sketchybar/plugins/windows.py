#!/opt/homebrew/bin/python3

import os
import json

def get_current_space_index():
    space_obj = json.loads(os.popen("yabai -m query --spaces --space").read())
    return space_obj["index"]

def get_space_windows(idx):
    windows_obj = json.loads(os.popen("yabai -m query --windows --space "+str(idx)).read())
    return windows_obj

def clear_sketchybar_items_string():
    bar_obj = json.loads(os.popen("sketchybar --query bar").read())
    bar_items = bar_obj["items"]
    cmd = ""
    for item in bar_items:
        if item == "window_handler":
            continue
        cmd+=" --remove "+item
    return cmd
        

def add_sketchybar_items(windows):
    cmd = "sketchybar " + clear_sketchybar_items_string()
    for i,w in enumerate(windows):
        title = (w["title"][:28] + '..') if len(w["title"]) > 30 else w["title"]
        cmd+=" --add item window_"+str(i)+" left"
        cmd+=" --set window_"+str(i)+" label='"+title+"'"
        cmd+=" click_script='yabai -m window --focus "+str(w["id"])+"'"
        if w["has-focus"]:
            cmd+=" label.color=0xff7ed321"
    os.system(cmd)
    

   
    

idx=get_current_space_index()
windows = get_space_windows(idx)
#windows = sorted(windows,key=lambda x: x["id"])

print("---")
add_sketchybar_items(windows)
print("---")


