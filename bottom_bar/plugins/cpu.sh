#!/bin/bash

cpu_percent=$(top -l  2 | grep -E "^CPU" | tail -1 | awk '{ print $3 + $5"%" }')

bottom_bar --set $NAME icon=󰍛 label="${cpu_percent}"


