#!/bin/sh

TEMP=$(~/.config/sketchybar/plugins/temp_sensor)

# The item invoking this script (name $NAME) will get its icon and label
# updated with the current battery status
bottom_bar --set $NAME icon= label="${TEMP}°"
