#!/usr/bin/env sh

CURRENT_WIFI="$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I)"
SSID="$(echo "$CURRENT_WIFI" | grep -o "SSID: .*" | sed 's/^SSID: //')"


if [ "$SSID" = "" ]; then
  bottom_bar --set $NAME  icon=󰤮 label="DISC"
else
  bottom_bar --set $NAME  icon= label="CONN"
fi
