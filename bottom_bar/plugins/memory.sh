#!/bin/bash

MEMORY=$(memory_pressure | tail -n 1 | awk '{print $5}')

bottom_bar --set $NAME icon=󰍛 label="${MEMORY}"
